﻿using GameXO2.BaseLevel.Abstract;
using GameXO2.BaseLevel.Concrete;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GameXO2.WebLevel.Infrastructure
{

        public class NinjectDependencyResolver : IDependencyResolver
        {
            private IKernel kernel;
            private CurrentGames games = new CurrentGames();
            private UsersOnline users = new UsersOnline();

            public NinjectDependencyResolver(IKernel kernelParam)
            {
                kernel = kernelParam;
                AddBindings();
            }

            public object GetService(Type serviceType)
            {
                return kernel.TryGet(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                return kernel.GetAll(serviceType);
            }

            private void AddBindings()
            {
                kernel.Bind<IUserRepository>().To<EFUserRepository>();
                kernel.Bind<ICurrentGames>().ToConstant(games);
                kernel.Bind<IUsersOnline>().ToConstant(users);
            }
        }

    
}