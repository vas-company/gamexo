﻿namespace GameXO2.WebLevel
{
    using System.Collections.Generic;
    using Microsoft.AspNet.SignalR;
    using Microsoft.AspNet.SignalR.Hubs;
    using GameXO2.WebLevel.Controllers;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security;
    using System;
    using GameXO2.BaseLevel.Abstract;
    using GameXO2.BaseLevel;
    using GameXO2.WebLevel.App_LocalResources;
    using System.Web;

    [Authorize]
    [HubName("userOnlineHub")]
    public class UserOnlineHub : Hub
    {
        //Языковые массивы для перевода JS
        #region(LanguageJS)
        public string[] langTextShowAsk = new string[] {
            GlobalRes.AreYouRedyText,
            GlobalRes.LetsStartWarText,
            GlobalRes.YesGoText,
            GlobalRes.NotNowText
 
        };

        public string[] langTextAskOK = new string[] {
            GlobalRes.AcceptWarText,
            GlobalRes.GoodText,
            GlobalRes.BeginingText
        };

        public string[] langTextAskNO = new string[] 
              {
            GlobalRes.WarNotStartText,
            GlobalRes.ExcellentText,
            GlobalRes.LetsSleep
         };

        public string[] langTextApponentHod = new string[] {
            GlobalRes.YouNextText,
            GlobalRes.StopGame
        };

        public string[] langTextApponentHodWait = new string[] {
            GlobalRes.WaitApponentText,
            GlobalRes.StopGame
         };


        public string[] langTextWin = new string[] {
            GlobalRes.YouWinText,
            GlobalRes.LetsRepeatText,
            GlobalRes.YesGoText,
            GlobalRes.NotNowText,
            GlobalRes.WaitApponentText,
             GlobalRes.StopGame
        };

        public string[] langTextLose = new string[] {
            GlobalRes.YoyLoseText,
            GlobalRes.LetsTryAgainText,
            GlobalRes.YesGoText,
            GlobalRes.NotNowText,
            GlobalRes.WaitApponentText,
             GlobalRes.StopGame
         };

        public string[] langTextNobody = new string[] {
            GlobalRes.NobodyWinText,
            GlobalRes.LetsWinThemText,
            GlobalRes.YesGoText,
            GlobalRes.NotNowText,
            GlobalRes.WaitApponentText,
             GlobalRes.StopGame
         };
        public string[] langTextRepeatWait = new string[] {
             GlobalRes.WaitApponentText,
             GlobalRes.StopGame
        };
        public string[] langTextAlreadyInGame = new string[] {
             GlobalRes.AlreadyInGameText,
             GlobalRes.NextText
        };
        public string[] langTextInitAsk = new string[] {
            GlobalRes.WaitTenSecText,
            GlobalRes.LetsWaitText,
            GlobalRes.StopGame,
            GlobalRes.NotWinSelfText,
            GlobalRes.NextText
         };

        public string[] langTextInitStopGame = new string[] {
            GlobalRes.RealyAbortGameText,
            GlobalRes.YesText,
            GlobalRes.NoText
        };

        public string[] langTextInitLogOff = new string[] {
            GlobalRes.RealyExitText,
            GlobalRes.YesText,
            GlobalRes.NoText
         };
        #endregion

        //Промежуточные методы для старта вызова функций от имени другого клиента
        #region(MethodsUserContext)
        [HubMethodName("showAskUserContext")]
        public void ShowAskUserContext(dynamic loginForGame, dynamic emailForGame)
        {
            Clients.Caller.showAsk(loginForGame, emailForGame, langTextShowAsk);
        }

        [HubMethodName("askOKUserContext")]
        public void AskOKUserContext(dynamic loginForGame)
        {
            Clients.Caller.askOK(loginForGame, langTextAskOK);
        }

        [HubMethodName("askNOUserContext")]
        public void AskNOUserContext()
        {
            Clients.Caller.askNO(langTextAskNO);
        }

        [HubMethodName("apponentHodUserContext")]
        public void ApponentHodUserContext(dynamic value, dynamic type, dynamic curTypeHod)
        {
            Clients.Caller.apponentHod(value, type, curTypeHod, langTextApponentHod);
        }

        [HubMethodName("apponentHodWaitUserContext")]
        public void ApponentHodWaitUserContext()
        {
            Clients.Caller.apponentHodWait(langTextApponentHodWait);
        }

        [HubMethodName("winUserContext")]
        public void WinUserContext(dynamic loginForGame, dynamic emailForGame)
        {
            Clients.Caller.win(loginForGame, emailForGame, langTextWin);
        }

        [HubMethodName("loseUserContext")]
        public void LoseUserContext(dynamic loginForGame, dynamic emailForGame)
        {
            Clients.Caller.lose(loginForGame, emailForGame, langTextLose);
        }

        [HubMethodName("nobodyUserContext")]
        public void NobodyUserContext(dynamic loginForGame, dynamic emailForGame)
        {
            Clients.Caller.nobody(loginForGame, emailForGame, langTextNobody);
        }

        [HubMethodName("initAskMessageUserContext")]
        public void InitAskMessageUserContext(dynamic init, dynamic loginForAsk)
        {
            Clients.Caller.initAskMessage(init, loginForAsk, langTextInitAsk);
        }

        [HubMethodName("initStopGameMessageUserContext")]
        public void InitStopGameMessageUserContext()
        {
            Clients.Caller.initStopGameMessage(langTextInitStopGame);
        }
        #endregion

        /// <summary>
        /// Обновление отображения основных областей у всех клиентов
        /// </summary>
        public void UpdateView()
        {
            var hrefUsersOnline = "/Room/GameUserList";
            var hrefCurrentUser = "/Room/CurrentGameUser";
            var hrefLogOff = "/Room/LogOff";
            var hrefnolabel = "/Room/GameNoLabel";
            Clients.All.updateUsersOnline(hrefUsersOnline, hrefCurrentUser, hrefLogOff, hrefnolabel);
        }

        /// <summary>
        /// Обновление отображения основных областей у вызывающего
        /// </summary>
        public void UpdateViewCaller()
        {
            var hrefUsersOnline = "/Room/GameUserList";
            var hrefCurrentUser = "/Room/CurrentGameUser";
            var hrefLogOff = "/Room/LogOff";
            var hrefnolabel = "/Room/GameNoLabel";
            Clients.Caller.updateUsersOnline(hrefUsersOnline, hrefCurrentUser, hrefLogOff, hrefnolabel);
        }

        public override System.Threading.Tasks.Task OnConnected()
        {
            RoomController.UserConnect(Context.User.Identity.GetUserId());
            UpdateView();
            return base.OnConnected();
        }


        public override System.Threading.Tasks.Task OnReconnected()
        {
            RoomController.UserConnect(Context.User.Identity.GetUserId());
            UpdateView();
            return base.OnReconnected();
        }


        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            RoomController.UserDisconnect(Context.User.Identity.GetUserId());
            StopGame();
            UpdateView();
            return base.OnDisconnected(stopCalled);
        }

        /// <summary>
        /// Установка языка во время игры
        /// </summary>
        /// <param name="Language">язык ru en</param>
        [HubMethodName("languageSet")]
        public void LanguageSet(string Language)
        {
            System.Web.HttpContextBase httpContext = Context.Request.GetHttpContext();
            if (Language != null)
            {
                HttpCookie cookie = httpContext.Request.Cookies["gameculture"];
                if (cookie == null)                    
                {

                    cookie = new HttpCookie("gameculture");
                    cookie.HttpOnly = false;
                    cookie.Value = Language; 
                    httpContext.Response.Cookies.Add(cookie);
                }               
            }
            UpdateViewCaller();
        }

        /// <summary>
        /// Внутренний метод для инициализации игры
        /// </summary>
        /// <param name="gO">Игрок О</param>
        /// <param name="gX">Игрок Х</param>
        private void InitGame(GameUser gO, GameUser gX)
        {
            Game game = RoomController.AddGame(gX, gO);
            if (game != null)
            {
                game.Type = 1;
                game.Value = 0;
                game.CurrentHodId = gX.UserID;
                game.RepeatCount = 0;
                game.RepeatStatus = false;
            }
        }


        /// <summary>
        /// Начать процесс запроса подтверждения на игру
        /// </summary>
        /// <param name="myLogin">Логин начинающего игру</param>
        /// <param name="loginForAsk">Логин оппонента</param>
        /// <param name="emailForAsk">Почта оппонента</param>
        [HubMethodName("startAsk")]
        public void StartAsk(string myLogin, string loginForAsk, string emailForAsk)
        {
            //Проверка вызвал ли игрок сам себя
            if (myLogin == loginForAsk)
            {
                Clients.Caller.initAskMessage(false, null, langTextInitAsk);
            }
            else
            {
                GameUser gO = RoomController.GetUser(Context.User.Identity.GetUserId());
                GameUser gX = RoomController.GetUserForLogin(loginForAsk);
                //Проверка вызвал игрок уже играющего или играет сам
                if (RoomController.GameExist(gX, gO))
                {
                    Clients.Caller.alreadyInGame(langTextAlreadyInGame);
                }
                else
                {
                    
                    InitGame(gO, gX);
                    Clients.Caller.initAskMessage(true, loginForAsk, langTextInitAsk);
                    Clients.User(emailForAsk).showAskUserContext(gO.Login, gO.Email);
                }
            }
        }

        /// <summary>
        /// Обработка ответа подтверждения игры
        /// </summary>
        /// <param name="confirm">Статус подтерждения</param>
        /// <param name="loginForGame">Логин начившего игру</param>
        /// <param name="emailForGame">Почта начившего игру</param>
        [HubMethodName("confirmAsk")]
        public void ConfirmAsk(bool confirm, string loginForGame, string emailForGame)
        {

            string userID = Context.User.Identity.GetUserId();
            if (confirm)
            {
                GameUser user = RoomController.GetUser(userID);
                var hrefInit = "/Room/StartGame?IDx=" + user.UserID + "&InitFlag=true";
                var hrefConfirm = "/Room/StartGame?IDx=" + Guid.Empty + "&InitFlag=false";
                Clients.User(emailForGame).showGameArea(hrefInit);
                Clients.User(emailForGame).askOKUserContext(user.Login);
                Clients.Caller.showGameArea(hrefConfirm);               
            }
            else
            {
                Clients.User(emailForGame).askNOUserContext();
                RoomController.RemoveGame(userID);
            }
        }

        /// <summary>
        /// Обработка ответа подтверждения повторной игры
        /// </summary>
        /// <param name="confirm">Статус подтерждения</param>
        /// <param name="loginForGame">Логин оппонента</param>
        /// <param name="emailForGame">Почта оппонента</param>
        [HubMethodName("confirmRepeatAsk")]
        public void ConfirmRepeatAsk(bool confirm, string loginForGame, string emailForGame)
        {
            string userID = Context.User.Identity.GetUserId();
            if (confirm)
            {
                Game game = RoomController.GetGame(userID);
                if (game != null)
                    if (game.RepeatCount == 1)
                    {
                        RoomController.RemoveGame(userID);
                        GameUser gX = RoomController.GetUser(userID);
                        GameUser gO = RoomController.GetUserForLogin(loginForGame);
                        InitGame(gO, gX);
                        var hrefInit = "/Room/StartGame?IDx=" + gX.UserID + "&InitFlag=true";
                        var hrefConfirm = "/Room/StartGame?IDx=" + Guid.Empty + "&InitFlag=false";
                        Clients.User(emailForGame).showGameArea(hrefInit);
                        Clients.User(emailForGame).askOKUserContext(gX.Login);
                        Clients.Caller.showGameArea(hrefConfirm);                        
                    }
                    else
                    {
                        Clients.Caller.initRepeatWait(langTextRepeatWait);
                        game.RepeatCount++;
                    }
            }
            else
            {
                Clients.User(emailForGame).askNOUserContext();
                RoomController.RemoveGame(userID);
            }
        }

        /// <summary>
        /// Обработка хода игрока и передача хода партнёру
        /// </summary>
        /// <param name="Value">Значение клетки на поле</param>
        /// <param name="Type">Тип хода 0 1</param>
        [HubMethodName("gameHod")]
        public void GameHod(byte Value, byte Type)
        {
            string userID = Context.User.Identity.GetUserId();
            Game game = RoomController.GetGame(userID);
            if (game != null)
                if (Guid.Parse(userID) == game.CurrentHodId)
                {
                    game.Type = Type;
                    game.Value = Value;
                    Clients.Caller.apponentHodWait(langTextApponentHodWait);
                    if (Type == 1)
                    {
                        game.CurrentHodId = game.O.UserID;
                        Clients.User(game.O.Email).apponentHodUserContext(Value, Type, 0);
                    }
                    else
                    {
                        game.CurrentHodId = game.X.UserID;
                        Clients.User(game.X.Email).apponentHodUserContext(Value, Type, 1);
                    }
                }
        }

        /// <summary>
        /// Обработка случая победы
        /// </summary>
        /// <param name="Type">Тип победителя 0 1</param>
        [HubMethodName("gameWin")]
        public void GameWin(byte Type)
        {
            string userID = Context.User.Identity.GetUserId();
            Game game = RoomController.GetGame(userID);
            if (Type == 1)
            {
                RoomController.SetUserWin(game.X.UserID, game.O.UserID);
                Clients.User(game.X.Email).winUserContext(game.O.Login, game.O.Email);
                Clients.User(game.O.Email).loseUserContext(game.X.Login, game.X.Email);
            }
            else
            {
                RoomController.SetUserWin(game.O.UserID, game.X.UserID);
                Clients.User(game.O.Email).winUserContext(game.X.Login, game.X.Email);
                Clients.User(game.X.Email).loseUserContext(game.O.Login, game.O.Email);
            }
             UpdateView();

        }

        /// <summary>
        /// Обработка слуая ничьи
        /// </summary>
        [HubMethodName("gameWinNobody")]
        public void GameWinNobody()
        {
            string userID = Context.User.Identity.GetUserId();
            Game game = RoomController.GetGame(userID);
            RoomController.SetUserNobody(game.X.UserID, game.O.UserID);
            UpdateView();
            Clients.User(game.X.Email).nobodyUserContext(game.O.Login, game.O.Email);
            Clients.User(game.O.Email).nobodyUserContext(game.X.Login, game.X.Email);
        }

        /// <summary>
        /// Сообщение на подтверждение при завершении игры
        /// </summary>
        [HubMethodName("initStopGame")]
        public void InitStopGame()
        {
            Clients.Caller.initStopGameMessage(langTextInitStopGame);
        }

        /// <summary>
        /// Завершение игры
        /// </summary>
        [HubMethodName("stopGame")]
        public void StopGame()
        {
            string userID = Context.User.Identity.GetUserId();
            Game game = RoomController.GetGame(userID);
            if (game != null)
            {
                if (game.X.UserID == Guid.Parse(userID))
                {
                    Clients.User(game.O.Email).askNOUserContext();
                }
                else
                {
                    Clients.User(game.X.Email).askNOUserContext();
                }
                RoomController.RemoveGame(userID);
            }
        }

        /// <summary>
        /// Сообщение на подтверждение при выходе из игры
        /// </summary>
        [HubMethodName("initLogOff")]
        public void InitLogOff()
        {
            Clients.Caller.initLogOffMessage(langTextInitLogOff);
        }
    }
}