﻿using GameXO2.BaseLevel.Abstract;
using GameXO2.WebLevel.App_LocalResources;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace GameXO2.WebLevel.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "EmailEnterError")]
        [Display(ResourceType = typeof(GlobalRes), Name = "EmailDisplay")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PasswordEnterError")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(GlobalRes), Name = "PasswordDisplay")]
        public string Password { get; set; }
    }

    public class RegisterViewModel
    {
        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "LoginEnterError")]
        [MinLength(3, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "LoginMinError")]
        [MaxLength(20, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "LoginMaxError")]
        [Display(ResourceType = typeof(GlobalRes), Name = "LoginDisplay")]
        public string Login { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "EmailEnterError")]
        [EmailAddress]
        [Display(ResourceType = typeof(GlobalRes), Name = "EmailDisplay")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PasswordEnterError")]
        [MinLength(6, ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PasswordMinError")]
        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(GlobalRes), Name = "PasswordDisplay")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(ResourceType = typeof(GlobalRes), Name = "PasswordConfDisplay")]
        [Compare("Password", ErrorMessageResourceType = typeof(GlobalRes), ErrorMessageResourceName = "PasswordConfError")]
        public string ConfirmPassword { get; set; }
    }
}
