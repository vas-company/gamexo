﻿using System.Web;
using System.Web.Mvc;

namespace GameXO2.WebLevel
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
