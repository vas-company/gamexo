﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using GameXO2.BaseLevel.Abstract;
using GameXO2.BaseLevel;
using GameXO2.WebLevel.Models;
using System.Web.Security;
using System.Collections.Generic;
using GameXO2.WebLevel.App_LocalResources;

namespace GameXO2.WebLevel.Controllers
{
    [Authorize]
    public class RoomController : Controller
    {
        private static IUsersOnline UsersOnline;
        private static IUserRepository UserRep;
        private static ICurrentGames CurrentGames;

        public RoomController(IUsersOnline usersOnline, IUserRepository userRep, ICurrentGames currentGames)
        {
            UsersOnline = usersOnline;
            UserRep = userRep;
            CurrentGames = currentGames;
        }

        public ViewResult GameRoom()
        {
            //Установка флага видимости игрового поля
            ViewBag.GameShow = false;
            return View();
        }

        /// <summary>
        /// Подключение нового пользователя
        /// </summary>
        /// <param name="userID">ID  пользователя</param>
        public static bool UserConnect(string userID)
        {
            if (UserRep != null)
                return UsersOnline.Connect(Guid.Parse(userID), UserRep);
            else return false;
        }

        /// <summary>
        /// Отключение пользователя
        /// </summary>
        /// <param name="userID"></param>
        public static void UserDisconnect(string userID)
        {
            if (UserRep != null)
                 UsersOnline.Disconnect(Guid.Parse(userID));    
        }

        /// <summary>
        /// Проверка по почте в сети ли пользователь 
        /// </summary>
        /// <param name="email">Почта</param>
        public static bool CheckUserOnline(string email)
        {
            if (UsersOnline != null)
            {
                var u = UsersOnline.Users.Where(p => p.Email == email);
                if (u.Count() > 0)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Вернуть игрока из пользователей в сети по ID
        /// </summary>
        /// <param name="userID">ID  пользователя</param>
        public static GameUser GetUser(string userID)
        {
            if (UsersOnline != null)
            {
                var u=UsersOnline.Users.Where(p => p.UserID == Guid.Parse(userID));
                if(u.Count()>0)
                return u.First();
            }
            return null;
        }

        /// <summary>
        /// Вернуть игрока из пользователей в сети по логину
        /// </summary>
        /// <param name="userLogin">Логин</param>
        public static GameUser GetUserForLogin(string userLogin)
        {
            if (UsersOnline != null)
            {
                var u = UsersOnline.Users.Where(p => p.Login == userLogin);
                if (u.Count() > 0)
                    return u.First();
            }
            return null;
        }

        /// <summary>
        /// Обовить статистику пользователей при победе
        /// </summary>
        /// <param name="userIDWin">ID победителя</param>
        /// <param name="userIDLose">ID проигравшего</param>
        public static void SetUserWin(Guid userIDWin, Guid userIDLose)
        {
            UserRep.AddUserWins(userIDWin, userIDLose);
            UserDisconnect(userIDWin.ToString()); UserConnect(userIDWin.ToString());
            UserDisconnect(userIDLose.ToString()); UserConnect(userIDLose.ToString());
        }

        /// <summary>
        /// Обовить статистику пользователей когда ничья
        /// </summary>
        /// <param name="user1">Игрок 1</param>
        /// <param name="user2">Игрок 2</param>
        public static void SetUserNobody(Guid user1, Guid user2)
        {
            UserRep.AddUserWinsNobody(user1, user2);
            UserDisconnect(user1.ToString()); UserConnect(user1.ToString());
            UserDisconnect(user2.ToString()); UserConnect(user2.ToString());
        }

        /// <summary>
        /// Отобразить список пользователей в сети
        /// </summary>
        public PartialViewResult GameUserList()
        {
            return PartialView(UsersOnline.Users);
        }

        public PartialViewResult LogOff()
        {
            return PartialView();
        }

        public PartialViewResult GameNoLabel()
        {
            return PartialView();
        }

        /// <summary>
        /// Отобразить статус текущего игрока
        /// </summary>
        public PartialViewResult CurrentGameUser()
        {
            return PartialView(UserRep.GameUsers.Where(p => p.Email == User.Identity.Name).First());
        }

        /// <summary>
        /// Вернуть информацию об игре по ID  
        /// </summary>
        /// <param name="userID">ID  пользователя</param>
        public static Game GetGame(string userID)
        {
            if (CurrentGames != null)
                return CurrentGames.GetGame(Guid.Parse(userID));
            else return null;
        }

        /// <summary>
        /// Проверить существует ли игра
        /// </summary>
        /// <param name="gX">Игрок Х</param>
        /// <param name="gO">Игрок О</param>
        public static bool GameExist(GameUser gX, GameUser gO)
        {
            if (CurrentGames != null)
                return CurrentGames.GameExist( gX,gO);
            else return false;
        }

        /// <summary>
        /// Удалить игру
        /// </summary>
        /// <param name="userID">ID  пользователя</param>
        public static void RemoveGame(string userID)
        {
            CurrentGames.RemoveGame(Guid.Parse(userID));
        }

        /// <summary>
        /// Добавить игру
        /// </summary>
        /// <param name="gX">Игрок Х</param>
        /// <param name="gO">Игрок О</param>
        public static Game AddGame(GameUser gX, GameUser gO)
        {
            return CurrentGames.AddGame(gX, gO);
        }

        /// <summary>
        /// Начать процесс запуска игры и отображения игрового поля
        /// </summary>
        /// <param name="IDx">ID игрок X</param>
        /// <param name="InitFlag">Статус начала игры</param>
        public ActionResult StartGame(Guid IDx, bool InitFlag)
        {
            Guid userO = Guid.Parse(User.Identity.GetUserId());
            GameUser gO = UsersOnline.Users.First(u => u.UserID == userO);
            if (InitFlag)
            {
                GameUser gX = UsersOnline.Users.First(u => u.UserID == IDx);
                Microsoft.AspNet.SignalR.IHubContext context = Microsoft.AspNet.SignalR.GlobalHost.ConnectionManager.GetHubContext<UserOnlineHub>();
                context.Clients.User(gX.Email).startGame(type: 1, curTypeHod: 1);
                context.Clients.User(gO.Email).startGame(type: 0, curTypeHod: 1);
                ViewBag.TitleXO = GlobalRes.WaitApponentText;
            }
            else ViewBag.TitleXO = GlobalRes.YouNextText;
            ViewBag.GameShow = true;
            return PartialView("GameArea");
        }

    }
}