﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameXO2.BaseLevel.Abstract
{
    public interface IUserRepository
    {
        IEnumerable<GameUser> GameUsers { get; }
        void NewUser(GameUser gameUser);
        void AddUserWins(Guid winUser, Guid loseUser);
        void AddUserWinsNobody(Guid User1, Guid User2);
    }

    public interface IUsersOnline
    {
        IEnumerable<GameUser> Users { get; }
        bool Connect(Guid userID, IUserRepository userRep);
        void Disconnect(Guid userID);
    }

    public interface ICurrentGames
    {
        IEnumerable<Game> Games { get; }
        Game AddGame(GameUser x, GameUser o);
        Game GetGame(Guid userID);
        bool GameExist(GameUser x, GameUser o);        
        bool GameExist(Guid userID);
        void RemoveGame(GameUser x, GameUser o);
        void RemoveGame(Guid userID);
        void Clear();
        
    }

}
