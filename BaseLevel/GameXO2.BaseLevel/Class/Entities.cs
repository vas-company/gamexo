﻿using GameXO2.BaseLevel.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GameXO2.BaseLevel
{

    /// <summary>
    /// Содержит публичную информацию о пользователе
    /// </summary>
    public class GameUser
    {
        /// <summary>
        /// Идентификатор пользователя
        /// </summary>
        [IndexAttribute(IsClustered=false,IsUnique=true)]       
        public Guid UserID { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        [Required(ErrorMessage = "Введите логин")]
        [IndexAttribute(IsClustered=false,IsUnique=true)]
        [MinLength(3, ErrorMessage = "Не меньше 3х символов"), MaxLength(20, ErrorMessage = "Не больше 20 символов")]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        /// <summary>
        /// Почта
        /// </summary>
        [Required(ErrorMessage = "Введите почту")]
        [Key]
        [EmailAddress]
        [Display(Name = "Почта бойца")]
        public string Email { get; set; }

        /// <summary>
        /// Количество побед
        /// </summary>
        [Display(Name = "Победы")]
        [Range(0, int.MaxValue, ErrorMessage = "Пожалуйста, введите значение побед > 0")]
        public int WinsNumber { get; set; }

        /// <summary>
        /// Общее количество игр
        /// </summary>
        [Display(Name = "Всего игр")]
        [Range(0, int.MaxValue, ErrorMessage = "Пожалуйста, введите значение игр > 0")]
        public int GameNumber { get; set; }
    }

    /// <summary>
    /// Содержит информацию об игре, X - первый игрок
    /// </summary>
    public class Game
    {
        public GameUser X { get; set; }
        public GameUser O { get; set; }
        public Guid CurrentHodId { get; set; }
        public byte Value { get; set; }
        public byte Type { get; set; }
        public GameStatus Status{ get; set; }
        public bool RepeatStatus {get; set;}
        public byte RepeatCount { get; set; }
    }

    public enum GameStatus
    {
        Stop=0,
        Start=1,
    }   
   }
