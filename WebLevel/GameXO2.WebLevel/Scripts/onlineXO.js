﻿$(function () {
    //старт основногоо хаба SignalR для игры
    var userOnline = $.connection.userOnlineHub;
  
        $.connection.hub.start();
    //обновление основных областей в игре
    userOnline.client.updateUsersOnline = function (hrefUsersOnline, hrefCurrentUser, hreflogoff, hrefnolabel) {
        $("#gameusers").load(hrefUsersOnline);
        $("#currentuser").load(hrefCurrentUser);
        $("#logoff").load(hreflogoff);
        $("#gameAreaNoLabel").load(hrefnolabel);
    };

    //Сообщение на подтверждение начала игры
    userOnline.client.showAskUserContext = function (loginForGame, emailForGame) {
        $.connection.userOnlineHub.server.showAskUserContext(loginForGame, emailForGame);
    };
    userOnline.client.showAsk = function (loginForGame, emailForGame, langTextShowAsk) {
        swal({
            title: langTextShowAsk[0],
            text: loginForGame + " " + langTextShowAsk[1],
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextShowAsk[2],
            cancelButtonText: langTextShowAsk[3],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            userOnline.server.confirmAsk(isConfirm, loginForGame, emailForGame);
        });
    };

    //Загрузка игрового поля
    userOnline.client.showGameArea = function (href) {
        $("#gameAreaNoLabel").hide();
        $("#gameArea").load(href);
    };

    //Сообщение в случае согласия оппонента на игру
    userOnline.client.askOKUserContext = function (loginForGame) {
        $.connection.userOnlineHub.server.askOKUserContext(loginForGame);
    };
    userOnline.client.askOK = function (loginForGame, langTextAskOK) {
        swal({
            title: langTextAskOK[0],
            confirmButtonText: langTextAskOK[1],
            type: "success",
            text: langTextAskOK[2],
            closeOnConfirm: true,
            timer: 3000,
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    };

    //Сообщение в случае отказа оппонента на игру
    userOnline.client.askNOUserContext = function () {
        $.connection.userOnlineHub.server.askNOUserContext();
    };
    userOnline.client.askNO = function (langTextAskNO) {
        swal({
            title: langTextAskNO[0],
            confirmButtonText: langTextAskNO[1],
            type: "error",
            text: langTextAskNO[2],
            closeOnConfirm: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            backRoom();
        });
    };

    //Установка на поле ходя оппонента
    userOnline.client.apponentHodUserContext = function (value, type, curTypeHod) {
        $.connection.userOnlineHub.server.apponentHodUserContext(value, type, curTypeHod);
    };
    userOnline.client.apponentHod = function (value, type, curTypeHod, langTextApponentHod) {
        CurTypeHod = curTypeHod;
        PlaceApponentHod(value, type);
        $('#hod').text(langTextApponentHod[0]);
        $('#stopgamelabel').text(langTextApponentHod[1]);
    };

    //Обновление статуса текущего хода
    userOnline.client.apponentHodWaitUserContext = function () {
        $.connection.userOnlineHub.server.apponentHodWaitUserContext();
    };
    userOnline.client.apponentHodWait = function (langTextApponentHodWait) {
        $('#hod').text(langTextApponentHodWait[0]);
        $('#stopgamelabel').text(langTextApponentHodWait[1]);
    };

    //Установка начальных параметров игры
    userOnline.client.startGame = function (type, curTypeHod) {
        CurTypeHod = curTypeHod;
        Type = type;
    };

    //Сообщение о победе и предложение к повторной игре
    userOnline.client.winUserContext = function (loginForGame, emailForGame) {
        $.connection.userOnlineHub.server.winUserContext(loginForGame, emailForGame);
    };
    userOnline.client.win = function (loginForGame, emailForGame, langTextWin) {
        GameOver();
        swal({
            title: langTextWin[0],
            text: langTextWin[1] + " " + loginForGame + "?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextWin[2],
            cancelButtonText: langTextWin[3],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            if (!isConfirm) backRoom();
            userOnline.server.confirmRepeatAsk(isConfirm, loginForGame, emailForGame);
        });
    };

    //Сообщение о поражении и предложение к повторной игре
    userOnline.client.loseUserContext = function (loginForGame, emailForGame) {
        $.connection.userOnlineHub.server.loseUserContext(loginForGame, emailForGame);
    };
    userOnline.client.lose = function (loginForGame, emailForGame, langTextLose) {
        GameOver();
        swal({
            title: langTextLose[0],
            text: langTextLose[1] + " " + loginForGame + "?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextLose[2],
            cancelButtonText: langTextLose[3],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            if (!isConfirm) backRoom();
            userOnline.server.confirmRepeatAsk(isConfirm, loginForGame, emailForGame);
        });
    };

    //Сообщение о ничье и предложение к повторной игре
    userOnline.client.nobodyUserContext = function (loginForGame, emailForGame) {
        $.connection.userOnlineHub.server.nobodyUserContext(loginForGame, emailForGame);
    };
    userOnline.client.nobody = function (loginForGame, emailForGame, langTextNobody) {
        GameOver();
        swal({
            title: langTextNobody[0],
            text: langTextNobody[1] + " " + loginForGame + "?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextNobody[2],
            cancelButtonText: langTextNobody[3],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            if (!isConfirm) backRoom();
            userOnline.server.confirmRepeatAsk(isConfirm, loginForGame, emailForGame);
        });
    };

    //Сообщение об ошибке: выбранный игрок уже играет
    userOnline.client.alreadyInGame = function (langTextAlreadyInGame) {
        swal({
            title: langTextAlreadyInGame[0],
            confirmButtonText: langTextAlreadyInGame[1],
            type: "error",
            allowOutsideClick: false,
            closeOnConfirm: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    }

    //Сообщение об ожидании согласия оппонента на игру при повторной игре
    userOnline.client.initRepeatWait = function (langTextRepeatWait) {
        repeatWait(langTextRepeatWait);
    }

    //Сообщение для подтверждение о согласии на игру
    userOnline.client.initAskMessageUserContext = function (init, loginForAsk) {
        $.connection.userOnlineHub.server.initAskMessageUserContext(init, loginForAsk);
    };
    userOnline.client.initAskMessage = function (init, loginForAsk, langTextInitAsk) {
        if (init) {
            swal({
                title: langTextInitAsk[0],
                text: loginForAsk + " " + langTextInitAsk[1],
                confirmButtonText: langTextInitAsk[2],
                closeOnConfirm: true,
                allowOutsideClick: false,
                allowEscapeKey: false
            }, function (isConfirm) {
                backRoom();
                $.connection.userOnlineHub.server.stopGame();
            });
        }
        else swal({
            title: langTextInitAsk[3],
            confirmButtonText: langTextInitAsk[4],
            type: "info",
            closeOnConfirm: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        });
    };

    //Сообщение о подверждение завершения игры
    userOnline.client.initStopGameMessageUserContext = function () {
        $.connection.userOnlineHub.server.initStopGameMessageUserContext();
    };
    userOnline.client.initStopGameMessage = function (langTextInitStopGame) {
        swal({
            title: langTextInitStopGame[0],
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextInitStopGame[1],
            cancelButtonText: langTextInitStopGame[2],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            if (isConfirm) {
                backRoom();
                $.connection.userOnlineHub.server.stopGame();
            }
        });
    }

    //Сообщение о подверждение выхода игры
    userOnline.client.initLogOffMessageUserContext = function () {
        $.connection.userOnlineHub.server.initLogOffMessageUserContext();
    };
    userOnline.client.initLogOffMessage = function (langTextInitLogOff) {
        swal({
            title: langTextInitLogOff[0],
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: langTextInitLogOff[1],
            cancelButtonText: langTextInitLogOff[2],
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            allowEscapeKey: false
        }, function (isConfirm) {
            if (isConfirm) {
                if (/mozilla/.test(navigator.userAgent.toLowerCase()) && !/webkit/.test(navigator.userAgent.toLowerCase()))
                    $('#logoutForm').submit();
                else {
                    $.connection.hub.stop();
                    $('#logoutForm').submit();
                }
            }
        });
    }
})

//Запуск игры
function initAskStart(loginForAsk, emailForAsk) {
    $.connection.userOnlineHub.server.startAsk($('#myLogin').text(), loginForAsk, emailForAsk);
}

//Сообщение об ожидании оппонента 
function repeatWait(langTextRepeatWait) {
    swal({
        title: langTextRepeatWait[0],
        confirmButtonText: langTextRepeatWait[1],
        type: "success",
        closeOnConfirm: true,
        allowOutsideClick: false,
        allowEscapeKey: false
    }, function (isConfirm) {
        $.connection.userOnlineHub.server.stopGame();
        backRoom();
    });
}

//Начало завершения игры
function initStopGame() {
    $.connection.userOnlineHub.server.initStopGame();
}

//Завершение игры
function stopGame() {
    $.connection.userOnlineHub.server.stopGame();
}

//Начало смены языка
function initLanguageSet(lang) {
    $.cookie("gameculture", lang, { path: '/' });
    $.connection.userOnlineHub.server.languageSet(lang);
}

//Начало выхода из игры
function initLogOffStart() {
    $.connection.userOnlineHub.server.initLogOff();
}

//Вернуться к выбору игрока, заврешив текущую игру
function backRoom() {
    GameOver();
    location.reload(true);
};