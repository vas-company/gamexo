﻿using GameXO2.BaseLevel.Abstract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameXO2.BaseLevel.Concrete
{
    public class XODbContext : DbContext
    {
        public DbSet<GameUser> GameUsers { get; set; }
    }

    public class EFUserRepository : IUserRepository
    {
        XODbContext context = new XODbContext();

        public IEnumerable<GameUser> GameUsers
        {
            get { return context.GameUsers; }
        }

        /// <summary>
        /// Добавить информацию о новом пользователе
        /// </summary>
        /// <param name="user">Новый пользователь</param>
        public void NewUser(GameUser gameUser)
        {
            if (gameUser.UserID != null)
            {
                context.GameUsers.Add(gameUser);
                context.SaveChanges();
            }

        }

        /// <summary>
        /// Обновить список побед пользователя
        /// </summary>
        /// <param name="winUser">Победитель ID</param>
        /// <param name="loseUser">Проигравший ID</param>
        public void AddUserWins(Guid winUser, Guid loseUser)
        {
            if (winUser != null && loseUser != null)
            {
                GameUser win = context.GameUsers.Where(p => p.UserID == winUser).First();
                GameUser lose = context.GameUsers.Where(p => p.UserID == loseUser).First();
                if (win != null && lose != null)
                {
                    win.GameNumber++;
                    win.WinsNumber++;
                    lose.GameNumber++;
                    context.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Ничья
        /// </summary>
        /// <param name="User1">Первый игрок ID</param>
        /// <param name="User2">Второй игрок ID</param>
        public void AddUserWinsNobody(Guid User1, Guid User2)
        {
            GameUser user1 = context.GameUsers.Where(p => p.UserID == User1).First();
            GameUser user2 = context.GameUsers.Where(p => p.UserID == User2).First();
            if (user1 != null && user2 != null)
            {                
                user1.GameNumber++;
                user2.GameNumber++;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Обновить список побед пользователя
        /// </summary>
        /// <param name="winUser">Победитель Email</param>
        /// <param name="loseUser">Проигравший Email</param>
        public void AddUserWins(string winUser, string loseUser)
        {
            if (winUser != null && loseUser != null)
            {
                GameUser win = context.GameUsers.Where(p => p.Email == winUser).First();
                GameUser lose = context.GameUsers.Where(p => p.Email == loseUser).First();
                if (win != null && lose != null)
                {
                    win.GameNumber++;
                    win.WinsNumber++;
                    lose.GameNumber++;
                    context.SaveChanges();
                }
            }
        }
    }

    /// <summary>
    /// Содержит информацию о текущих играх
    /// </summary>
    public class CurrentGames : ICurrentGames
    {
        public IEnumerable<Game> Games
        {
            get { return games; }
        }

        private List<Game> games = new List<Game>();

        public bool GameExist(GameUser x, GameUser o)
        {
            if (games.Count > 0)
                return games.Exists(g => g.X.UserID == x.UserID || g.O.UserID == x.UserID
                   || g.X.UserID == o.UserID || g.O.UserID == o.UserID);
            else return false;
        }

        public Game GetGame(Guid userID)
        {
            if (games.Count > 0)
                return games.First(g => g.X.UserID == userID || g.O.UserID == userID);
            else return null;
        }

        public bool GameExist(Guid userID)
        {
            return games.Exists(g => g.X.UserID == userID || g.O.UserID == userID);
        }

        public Game AddGame(GameUser x, GameUser o)
        {
            Game game = new Game
            {
                X = x,
                O = o,
                CurrentHodId = x.UserID,
                Type = 1,
                Value = 0,
                Status = GameStatus.Start,
            };
            games.Add(game);
            return game;
        }

        public void RemoveGame(GameUser x, GameUser o)
        {
            games.RemoveAll(g => g.X.UserID == x.UserID || g.O.UserID == x.UserID
                   || g.X.UserID == o.UserID || g.O.UserID == o.UserID);
        }

        public void RemoveGame(Guid userID)
        {
            games.RemoveAll(g => g.X.UserID == userID || g.O.UserID == userID);
        }

        public void Clear()
        {
            games.Clear();
        }


    }

    /// <summary>
    /// Содержит список игроков онлайн
    /// </summary>
    public class UsersOnline : IUsersOnline
    {
        public IEnumerable<GameUser> Users
        {
            get { return users; }
        }

        private List<GameUser> users = new List<GameUser>();

        public bool Connect(Guid userID, IUserRepository userRep)
        {
            GameUser gameUser = userRep.GameUsers.First(p => p.UserID == userID);
            if (!users.Exists(p => p.UserID == gameUser.UserID))
            {
                users.Add(gameUser);
                return true;
            }
            else return false;
        }

        public void Disconnect(Guid userID)
        {
            users.RemoveAll(p => p.UserID == userID);
        }
    }
}
